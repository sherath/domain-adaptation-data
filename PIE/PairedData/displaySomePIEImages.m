% Draw the source and the target domain images when domain names are given
clc
clear
close all

sourceName = 'Set7';
targetName = 'Set5';

numberOfImages = 25;
load([pwd '/' sourceName '-' targetName '/1.mat'])
%% Show some source domain images

X = trainData.X{1};
shuffle = randperm(length(trainData.labels{1}));
X = X(:,shuffle);
for nIm = 1 : numberOfImages
    
    subplot(2*5,5,nIm);
    
    im = reshape(X(:,nIm),[32 32]);
    imshow(im,[]);
    
end
title(['Source Domain :' sourceName])

%% Show target domain images
figure
X = testData.X{2};
shuffle = randperm(length(testData.labels{2}));
X = X(:,shuffle);
for nIm = 1 : numberOfImages
    
    subplot(2*5,5,nIm);
    
    im = reshape(X(:,nIm),[32 32]);
    imshow(im,[]);
    
end

title(['Target Domain :' targetName])

